package com.atlassian.marketplace.marketplace.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.repo.AddressRepository;

@Service
public class AddressService {

    private AddressRepository addressRepository;

    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    public void createAddress(Address addreess) {
        if (addressRepository.findById(addreess.getAddressId()) != null) {
            throw new RuntimeException("Address already exists.");
        }
        addressRepository.save(addreess);
    }
}
