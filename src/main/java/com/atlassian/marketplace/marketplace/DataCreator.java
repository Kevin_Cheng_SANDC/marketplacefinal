package com.atlassian.marketplace.marketplace;

public interface DataCreator {
    void createData();
}
