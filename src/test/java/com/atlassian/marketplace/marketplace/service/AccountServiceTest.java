package com.atlassian.marketplace.marketplace.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.atlassian.marketplace.marketplace.domain.AccountType;
import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.repo.AccountRepository;
import com.atlassian.marketplace.marketplace.repo.AddressRepository;
import com.atlassian.marketplace.marketplace.repo.ContactRepository;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;
    @Mock
    private AddressRepository addressRepository;
    @Mock
    private ContactRepository contactRepository;
    @InjectMocks
    private AccountService accountService;
    private static final String COMPANY_NAME = "AtlassianMarketPlace";

    @Test
    void createAccountWithNoContactsThrowsException() {
        Set<Contact> contacts = new HashSet<>();
        assertThrows(RuntimeException.class, ()-> accountService.createAccount(contacts, COMPANY_NAME, null, AccountType.ENTERPRISE));
    }

    @Test
    void createAccountWithNoExistingAccountsThrowsException() {
        Set<Contact> contacts = new HashSet<>();
        Contact contact = Mockito.mock(Contact.class);
        Address address = Mockito.mock(Address.class);
        contacts.add(contact);
        when(contact.getEmail()).thenReturn("kevin.cheng@workday.com");
        when(contactRepository.findById("kevin.cheng@workday.com")).thenReturn(Optional.empty());
        assertThrows(RuntimeException.class, ()-> accountService.createAccount(contacts, COMPANY_NAME, address, AccountType.ENTERPRISE));
    }

    @Test
    void lookUpReturnsAllAccounts() {
        accountService.lookup();
        verify(accountRepository, times(1)).findAll();
    }

    @Test
    void totalReturnsCount() {
        when(accountRepository.count()).thenReturn(100l);
        long actualCount = accountService.total();
        verify(accountRepository, times(1)).count();
        assertEquals(actualCount, 100l);
    }

    @Test
    void createAccountSavesAddress() {
        Set<Contact> contacts = new HashSet<>();
        Contact contact = Mockito.mock(Contact.class);
        contacts.add(contact);
        when(contact.getEmail()).thenReturn("kevin.cheng@workday.com");
        when(contactRepository.findById("kevin.cheng@workday.com")).thenReturn(Optional.of(contact));
        Address address = Mockito.mock(Address.class);
        accountService.createAccount(contacts, COMPANY_NAME, address, AccountType.ENTERPRISE);
        verify(addressRepository,times(1)).save(address);
    }
}