package com.atlassian.marketplace.marketplace.web.exceptions;

public class AccountDoesNotExistException extends RuntimeException {

    public AccountDoesNotExistException(String msg) {
        super(msg);
    }
}
