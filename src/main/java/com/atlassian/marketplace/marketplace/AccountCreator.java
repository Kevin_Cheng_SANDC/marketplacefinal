package com.atlassian.marketplace.marketplace;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.AccountType;
import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.AddressId;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.service.AccountService;
import com.atlassian.marketplace.marketplace.web.CompanyRatingsController;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Responsible for persisting Accounts into the DB.
 */
@Component
public class AccountCreator
    implements DataCreator {
    private AccountService accountService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountCreator.class);

    AccountCreator(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void createData() {
        createFirstAccount();
        createSecondAccount();
        createThirdAccount();
        createFourthAccount();
        createFifthAccount();
        LOGGER.info("created " + accountService.total() + " accounts in DB");
    }

    private void createFirstAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Workday Way").city("Pleasanton").state("California").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact("Kevin Cheng", "kevin@workday.com", null, null),
                new Contact("Henry Kim", "henry@workday.com", null, null),
                new Contact("VP of Sales", "sales@workday.com", null, null)));
        accountService.createAccount(contacts, "Workday", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createSecondAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Atlassian Way").city("Austin").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact("Jeff Marshall", "JeffMarshall@atlassian.com", null, null),
                new Contact("Recruiting", "recruiting@atlassian.com", null, null)));
        accountService.createAccount(contacts, "Atlassian", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createThirdAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Service Now").city("Dallas").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact("Joe Anderson", "JoeAnderson@servicenow.com", null, null)));
        accountService.createAccount(contacts, "ServiceNow", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createFourthAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Amazon Web Services").city("Plano").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact("Mary Lee", "MaryLee@amazonwebservices.com", null, null)));
        accountService.createAccount(contacts, "AWS", new Address(addressIdBuilder.build()),
                AccountType.ENTERPRISE);
    }

    private void createFifthAccount() {
        AddressId.Builder addressIdBuilder = new AddressId.Builder()
                .line1("Number1RobotInc").city("Plano").state("Texas").country("USA");
        Set<Contact> contacts = new HashSet<>(Arrays.asList(new Contact("Jamal Wilcox", "jwilcox@robostartup.com", null, null)));
        accountService.createAccount(contacts, "RoboStartup", new Address(addressIdBuilder.build()),
                AccountType.SMALL);
    }
}
