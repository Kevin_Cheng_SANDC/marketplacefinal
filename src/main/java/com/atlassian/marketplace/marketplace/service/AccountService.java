package com.atlassian.marketplace.marketplace.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.domain.AccountType;
import com.atlassian.marketplace.marketplace.domain.Address;
import com.atlassian.marketplace.marketplace.domain.AddressId;
import com.atlassian.marketplace.marketplace.domain.Contact;
import com.atlassian.marketplace.marketplace.repo.AccountRepository;
import com.atlassian.marketplace.marketplace.repo.AddressRepository;
import com.atlassian.marketplace.marketplace.repo.ContactRepository;
import com.atlassian.marketplace.marketplace.web.EntityDtoConverter;
import com.atlassian.marketplace.marketplace.web.AddressDto;
import com.atlassian.marketplace.marketplace.web.ContactDto;
import com.atlassian.marketplace.marketplace.web.exceptions.MissingContactException;

@Service
public class AccountService {

    private AccountRepository accountRepository;
    private AddressRepository addressRepository;
    private ContactRepository contactRepository;
    private EntityDtoConverter entityDtoConverter;

    @Autowired
    AccountService(AccountRepository accountRepository,
                   AddressRepository addressRepository,
                   ContactRepository contactRepository,
                   EntityDtoConverter entityDtoConverter) {
        this.accountRepository = accountRepository;
        this.addressRepository = addressRepository;
        this.contactRepository = contactRepository;
        this.entityDtoConverter = entityDtoConverter;
    }

    public void addAccountContacts(List<ContactDto> contactDtoList, String companyName) {
        Account account = accountRepository.findById(companyName).get();
        account.getContacts().addAll(entityDtoConverter.convert(contactDtoList, account));
        accountRepository.save(account);
    }

    public void updateAccountAddress(AddressDto addressDto, String companyName) {
        Account account = accountRepository.findById(companyName).get();
        AddressId addressId = entityDtoConverter.convert(addressDto);
        account.getAddress().setAddressId(addressId);
        account.setAccountType(addressDto.getAccountType());
        accountRepository.save(account);
    }

    public Account createAccount(Set<Contact> contacts, String companyName, Address address, AccountType accountType) {
        if (contacts == null || contacts.isEmpty()) {
            throw new RuntimeException("At least one contact is required to create an account.");
        }
        assertAllContactsExist(contacts);
        addressRepository.save(address);
        Account account = new Account(companyName, address, accountType, contacts);
        contacts.forEach(c -> c.setAccount(account));
        return accountRepository.findById(companyName)
                .orElse(accountRepository.save(account));
    }

    public void assertAllContactsExist(Set<Contact> contacts) {
        contacts.forEach(contact -> contactRepository.findById(contact.getEmail()).orElseThrow(() ->
                new MissingContactException("At least one contact does not exist.")));
    }

    public Iterable<Account> lookup() { return accountRepository.findAll(); };

    public long total() { return accountRepository.count(); };

    public Optional<Account> getExistingAccount(String companyName) {
        return accountRepository.findById(companyName);
    }
}
