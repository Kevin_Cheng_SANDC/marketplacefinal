package com.atlassian.marketplace.marketplace.web;
import static org.mockito.ArgumentMatchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.atlassian.marketplace.marketplace.domain.Account;
import com.atlassian.marketplace.marketplace.repo.AccountRepository;
import com.atlassian.marketplace.marketplace.repo.CompanyRatingsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class CompanyRatingsControllerTest {

    @Mock
    private CompanyRatingsRepository companyRatingsRepository;
    @Mock
    private AccountRepository accountRepository;
    @InjectMocks
    private CompanyRatingsController companyRatingsController;
    private MockMvc mockMvc;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;
    private ObjectMapper objectMapper = new ObjectMapper();
    private static final String URI = "/apps/{companyName}/ratings";

    @BeforeEach
    void before() {
        mockMvc  = MockMvcBuilders.standaloneSetup(companyRatingsController).build();
    }

    @Test
    public void testCreateCompanyRating() throws Exception {
        Optional<Account> accountOptional = Optional.of(Mockito.mock(Account.class));
        when(accountRepository.existsById("Workday")).thenReturn(true);
        when(accountRepository.findById("Workday")).thenReturn(accountOptional);
        CompanyRatingDto companyRatingDto = new CompanyRatingDto(1, "hello world", "1");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(URI, "Workday", request, response)
                .contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(companyRatingDto)))
                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();
        verify(companyRatingsRepository, times(1)).save(anyObject());
    }
}