package com.atlassian.marketplace.marketplace.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Contact {

    @Column
    private String name;
    @Id
    private String email;
    @OneToOne
    private Address address;
    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    private Account account;

    public Contact(String name, String email, Address address, Account account) {
        this.name = name;
        this.email = email;
        this.address = address;
        this.account = account;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Contact() {

    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
